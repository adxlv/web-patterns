const layoutAliases = [
	'base'
];


const staticPaths = {
	'src/assets': '/assets',
	'src/scripts': '/scripts',
	'src/favicon.svg': '/favicon.svg',
};


module.exports = function(eleventyConfig) {
	// layout aliases
	for (const alias of layoutAliases) {
		eleventyConfig.addLayoutAlias(alias, `${alias}.njk`);
	}

	// passthrough copies
	for (const url in staticPaths) {
		eleventyConfig.addPassthroughCopy({[url]: staticPaths[url]});
	}

	// browser sync for stylesheets
	eleventyConfig.setBrowserSyncConfig({
		files: './public/styles/**/*.css'
	});

	return {
		htmlTemplateEngine: "njk",
		dir: {
			input:		"src/views",
			includes:	"_includes", // relative to input directory
			data:		"_data",
			layouts:	"_layouts",
			output:		"public",
		}
	}
};