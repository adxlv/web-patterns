# An exploration of GNOME.org web patterns

(IN PROGRESS)

This is an exploration of a design language and web frontend framework, inpired by the principles of the GNOME platform and its [HIG](https://developer.gnome.org/hig/).

This exploration tries to answer the following question: "what if the [GNOME website](https://gnome.org), all its subdomains and collaboration tools could benefit from a shared user experience, so information access and collaboration are improved across the community?"

I started this as an experiment in the [adxlv/extensions-web-redesign-prototype](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype) repository, initially exploring Adwaita-inspired designs for the [Shell Extensions website](https://extensions.gnome.org). But after getting a few reusable patterns in mind, I decided to move away from a page-by-page custom implementation, and towards a framework-style implementation, exploring reusable components and patterns (and how they'd behave in other parts of GNOME's web presence).


## Initial design considerations

### Responsive and accessible by design

Patterns and components should be designed to be adaptive and accessible, so GNOME.org web experiences can work in any device, for everyone. This includes:

- working on small form factors (min size at 360×294px, see [HIG's Scaling &amp; Adaptiveness](https://developer.gnome.org/hig/guidelines/adaptive.html))
- using semantic and accessible markup and interactions (see [WAI-ARIA](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/WAI-ARIA_basics))
- Respect user preferences, including light/dark mode, high contrast ([prefers-contrast](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-contrast)), and matching to the system text size (see "CSS Units")


### Design both for content presentation and tooling

This exploration is considering designs and patterns targetting any GNOME website. This means both presentation (such as GNOME about pages, app and extension galleries), and internal/collaboration tools (such as Damned Lies).


### Play nice with existing infrastructure

This framework should be designed so it can be adopted by existing web architectures, be used in full or in parts, and retrofitted to existing pages with as little disruption as technically possible.

While some GNOME.org web experiences could benefit from a complete redesign during an eventual adoption of these patterns, iterative and small updates to existing GNOME.org subdomains would go a long way (for example, starting with updating main navigation headers and footers).


## Stylesheet structure

### `gw` initializer

A `gw` class (shorthand for `gnome-web`) wraps the framework stylesheets and sets its defaults without interfering with other parts of the page. One could adopt this framework only in specific parts of the markup, or in full if applied directly to the `<body>` tag, like so:

```html
<body class="gw">
```

I'm also using `gw` as a prefix for all CSS definitions. This aims to improve clarity in scenarios with multiple frameworks, and avoid naming collisions (such as with the [Deneb bootrstrap theme](https://wiki.gnome.org/GnomeWeb/Deneb)).

### Patterns

Patterns are basic building blocks of the user interface. It encapsulates any reusable controls, layout containers, navigation helpers, components, etc.

E.g.:
- `.gw-box`
- `.gw-headerbar`
- `.gw-button`
- `.gw-iconbutton`
- `.gw-boxedlist`

Patterns can have flag modifiers to change its visuals or behavior. Children nodes can target inner parts of the pattern.

Syntax:
- `.gw-[pattern]`
- `.gw-[pattern]--[modifier]`
- `.gw-[pattern]--[modifier]-[value]`
- `.gw-[pattern]-[node]`
- `.gw-[pattern]-[node]--[modifier]`
- `.gw-[pattern]-[node]--[modifier]-[value]`
- `.gw-[pattern]-[node]-[subnode]`
- etc.


E.g.: A GtkBox/CSS Flexbox equivalent with buttons inside:
```html
<div class="gw-box gw-box--horizontal gw-box--gap-small">
    <a class="gw-button" href="#">Previous</a>
    <a class="gw-button gw-button--suggested" href="#">Next</a>
</div>
```

E.g.: A header bar pattern
```html
<div class="gw-headerbar">
    <div class="gw-headerbar-title">
        <h1>
            <a href="/">
                <span class="gw-icon" data-gw-icon="gnome-logo">GNOME</span>
                <span>Localization</span>
            </a>
        </h1>
    </div>
    <div class="gw-headerbar-nav">
        <nav>...</nav>
    </div>
    <div class="gw-headerbar-actions">
        <button>...</button>
    </div>
```

### Generic modifiers

Generic modifiers aren't specific to any pattern, and can be applied anywhere, including the `gw` initializer.

Syntax:
- `.gw--[modifier]`
- `.gw--[modifier]-[value]`

E.g.:
- `.gw--pagewidth-[small|medium|large|full]`
- `.gw--theme-[light|dark]`
- `.gw--density-[small|medium|large]`

```html
<body class="gw gw--theme-dark">
```

<!--
Some generic modifiers work as utilities, facilitating adjusting single CSS properties, such as `padding` or `margin` for one-off layout adjustments.

E.g.:
- `.gw--margin-medium`
- `.gw--pad-horizontal-small`

Don't use single-property modifiers to create custom patterns. Write them directly in CSS instead. (FIXME: refer to CSS custom properties/variables)
-->

### Adaptive modifiers

Some modifiers can be conditionally applied to certain screen sizes. This can be used to make pages adapt their UI between large/desktop and small/mobile devices.

A named viewport is used as a predefined breakpoint. For now, only `narrow` is being considered, targetting mobile-friendly breakpoints (where width <= 768px). More exploration is needed for wider screens.

Syntax:
- `.gw--[viewport]--[modifier]-[value]`
- `.gw-[pattern]--[viewport]--[modifier]-[value]`
- `.gw-[pattern]-[node]--[viewport]--[modifier]-[value]`

E.g.: A Box component with buttons placed horizontally, adapted to show vertically on `narrow` viewports:

```html
<div class="gw-box gw-box--horizontal gw-box--narrow--vertical">
    <a class="gw-button" href="#">Previous</a>
    <a class="gw-button gw-button--suggested" href="#">Next</a>
</div>
```

### Data attributes

Some patterns may accept values passed as data attributes. All data attributes start with `data-gw-*`.

E.g: `<button class="gw-iconbutton" data-gw-icon="menu">Main menu</button>`

## Interactivity

Interactive components are necessary to keep pages accessible.

### Custom elements

For interactive components

E.g.
- `<gw-popover>`
- `<gw-dialog>`
- `<gw-menu>`?
- `<gw-messagedialog>`?
- `<gw-navigationsplitview>`?


<!--
---

DRAFTS BELOW

---

#### Generic modifiers

- `.gw--[modifier]-[value]`

- `.gw--pad-medium`
- `.gw--pad-block-medium`
- `.gw--pad-block-start-medium`

- `.gw--margin-large`
- `.gw--margin-inline-large`
- `.gw--margin-inline-start-large`

- `.gw--border-thin`
- `.gw--border-inline-thin`
- `.gw--border-inline-start-thin`

- `.gw--width-full`
- `.gw--width-page`
- `.gw--width-content`
- `.gw--width-sidebar`

Examples:

<figure>
<figurecaption>A GtkBox-like component</figurecaption>

```html
<div class="gw-box gw-box--horizontal gw-box--gap-medium">
    <a class="gw-button" href="#">Previous</a>
    <a class="gw-button" href="#">Next</a>
</div>
```
</figure>

<figure>
<figurecaption>A SplitView-style page layout</figurecaption>

```html
<div is="gw-split-view" data-gw-default-view="nav">
    <nav>...</nav>
    <main>...</main>
</div>
```
</figure>


## Develop

This repository currently uses [`eleventy`](https://www.11ty.dev/) to parse basic template functionality, with the [`nunjucks`](https://mozilla.github.io/nunjucks/) language.

-->

### CSS Units

TODO

`rem` by default

`px` as option for legacy scenarios (modify base-unit custom property to from `1rem` to `16px` to change).

When to use rem, when to use px.

GTK uses Android-inspired `sp` -- scale-independent pixels. Maybe introduce `--sp` variable so values can be provided as `calc(12 * var(--sp))` for 12sp? (--sp = 1/16 of 1rem)

Reference: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/enum.LengthUnit.html

## CSS Custom Properties (variables)



<!--

## Open questions

Interactivity and reliance of JavaScript

How opinionated are users of the GNOME websites to having pages functioning without JavaScript? 



---


- Use of `gw` prefix
- Option of a scoped application (only apply styles inside a scope-determiner `gw` class?)

To play nice with any existing stylesheet that may already exist in the page (particularly, compatible with Bootstrap, which is popular among GNOME.org subdomains through the [Deneb bootrstrap theme](https://wiki.gnome.org/GnomeWeb/Deneb)), all CSS definitions start with the `gw` prefix:

To play nice in any environment and avoid naming conflicts, all definitions start with the `gw` prefix:

custom CSS properties: `--gw-*`

Patterns and components: 

- `.gw-[pattern]`
- `.gw-[pattern]--[modifier]`

- `.gw-[pattern]-[item]`
- `.gw-[pattern]-[item]--[modifier]`

Data attributes:

- `data-gw-[pattern]="[value]"`
- `data-gw-[pattern]-[attribute]="[value]"`

Examples:

```html
<div class="gw_box gw_box--horizontal gw_box--horizontal@narrow">
    <a class="gw_button" href="#">Previous</a>
    <a class="gw_button" href="#">Next</a>
</div>
```

```html
<div class="gw-box gw-box--horizontal gw-box--gap-medium">
    <a class="gw-button" href="#">Previous</a>
    <a class="gw-button" href="#">Next</a>
</div>
```

data-gw-narrow-class="gw-box--gap-small"

- `.gw-box`
- `.gw-box--horizontal`
- `.gw-box--gap-medium`

- `data-gw-hidden="viewport-narrow"`
- `data-gw-theme="dark"`


`gw-box`
`gw-box--vertical`
`gw-box-gap-medium`
`--gw-box-gap: 20px`
`--gw-box-gap: calc(--gw-base-unit * 0.75)`


Question: Class vs. data attributes

### File structure

-->

### Patterns

**Controls**
- button
- iconbutton
- icon
- textfield
- dropdown

**Containers**
- page layout?
- card
- boxedlist
- popover
- dialog
- overlaid (osd)
- scroll-view

**Navigation**
- navlist
- menu
- search
- tabs

**Feedback**
- banner
- toast
- progressbar
- spinner
- dialog
- placeholder
- tooltip


#### Typography helpers

- rich-content
- title-[1-4]
- truncation
- numbered

#### Generic modifiers
- box
- hidden
- padding/margin
- sticky?


<!--
### Cascade layers
-->


<!--
## Running

Make sure you have `npm` >= 14.

- Run: `npm run start`
- Build `npm run build` (outputs to `_site` directory)
-->




## Reference

### GNOME subdomains

About
- www.gnome.org
- foundation.gnome.org
- release.gnome.org

Ecosystem
- apps.gnome.org
- circle.gnome.org
- extensions.gnome.org

Forums/Discussions
- discourse.gnome.org
- mail.gnome.org (archive)

Feeds
- planet.gnome.org
- blogs.gnome.org
- thisweek.gnome.org

Develop/Guides
- developer.gnome.org
- help.gnome.org
- wiki.gnome.org

Collaboration Tools
- l10n.gnome.org
- gitlab.gnome.org

Regional communities
- br.gnome.org
- cz.gnome.org

Other
- password.gnome.org
- status.gnome.org
- shop.gnome.org

See also:
- https://wiki.gnome.org/Websites
- https://wiki.gnome.org/GnomeWeb/GnomeSubsites

### Links

- [Original `extensions-web-redesign-prototype` repository](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype)