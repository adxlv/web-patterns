# An exploration: “GNOME Web Patterns”

*“What if the GNOME web presence and collaboration tools were designed to be as respectful and elegant as GNOME itself?”*

Inspired by the GNOME design tradition, I set out to explore how its [HIG](https://developer.gnome.org/hig/) and [Adwaita design language](https://gnome.pages.gitlab.gnome.org/libadwaita/) would translate to the web, for official [gnome.org](https://gnome.org) websites and tools.

This is an **⚠ in progress** exploration of both a design language and a frontend library. But more importantly, this is an exercise in [Information Architecture](https://en.wikipedia.org/wiki/Information_architecture), for serving GNOME's online community as a whole.

As of writing, I'm working on this on and off on a weekly basis. This README itself is still in flux.

For now, some info on how I'm building this exploration:

## Initial considerations

### A connected website of websites

This exploration pictures how to connect the different websites (subdomains) into a unified experience.

### Design both for content presentation and tooling

This exploration is considering designs targetting any GNOME website. This means both presentation (about pages, app and extension galleries), documentation, and internal/collaboration tools (such as Damned Lies).

### Friendly with existing infrastructure

While some GNOME.org web experiences could benefit from a redesign while adopting this design language, iterative and small updates to existing GNOME.org subdomains would go a long way (for example, adhering to unified main navigation headers and footers).

### Adaptive and accessible by design

- Works on small form factors (min size at 360×294px, see [HIG's Scaling &amp; Adaptiveness](https://developer.gnome.org/hig/guidelines/adaptive.html))
- Uses semantic and accessible markup and interactions (see [WAI-ARIA](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/WAI-ARIA_basics))
- Respects user preferences, including light/dark mode, high contrast ([prefers-contrast](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-contrast)), and matching to the system text size ("Large Text" accessibility setting)


## Design Language

**TODO: Share design explorations**

---

**⚠⚠⚠ In Progress**

## Frontend Library

Initially I was referring to this frontend implementation as a "Framework", as in a "[CSS Framework](https://en.wikipedia.org/wiki/CSS_framework)". But I realized this is a confusing term, which has little to do with [other computing definitions of "Framework"](https://en.wikipedia.org/wiki/Framework#Computing).

Calling it a "Library" seems more fitting for what I'm aiming for. This implementation aims to be nothing more than a CSS file you add to an HTML `<head>`, with JavaScript being used for [Progressive Enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement) and [ARIA accessibility](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA).

For building this library, I'm relying on:

- Using [Sass](https://sass-lang.com/), for organizing stylesheets
- Using [Eleventy](https://www.11ty.dev/) with [Nunjucks](https://mozilla.github.io/nunjucks/), for markup prototyping
- Using JavaScript for [Progressive Enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement) and [ARIA accessibility](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA), particularly by encapsulating behaviors with [Custom Elements](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements)

### Prefix

For now, using `gw` ("gnome web"?) as a global prefix, to avoid conflict with other styles.

Aiming it to be 2 chars-long, since it appears a lot. Also considering `gg`, an efficient clear single-key combo that feels easier to distinguish.

Open to suggestions (including naming suggestions).


### Style Encapsulation

A `.gw` class wraps the library styles, and sets its defaults without interfering with other parts of the page.

The library styles can be initialized anywhere inside an HTML page (to be used in parts of existing pages), or in full when applied to `<body>`. Example:

```html
<body class="gw">
```


## CSS Variables

Check [/src/styles/root.scss](https://gitlab.gnome.org/adxlv/web-patterns/-/blob/main/src/styles/root.scss?ref_type=heads)

For `light` and `dark` color schemes, check:
- [/src/styles/themes](https://gitlab.gnome.org/adxlv/web-patterns/-/tree/main/src/styles/themes?ref_type=heads)
- [/src/styles/base/_themes.scss](https://gitlab.gnome.org/adxlv/web-patterns/-/blob/main/src/styles/base/_themes.scss?ref_type=heads)

**TODO: Explain theming system**

## Pattern example

```html
<div class="gw-box gw-box--horizontal gw-box--narrow--vertical">
    <button class="gw-button">Cancel</button>
    <button class="gw-button gw-button--suggested">Submit</button>
</div>
```

## Component example

```html
<gw-mainheader>
    <h1 data-gw-slot="title">
        <a href="/examples/gnome.org/">
            <span class="gw-icon" data-gw-icon="gnome">GNOME</span>
        </a>
    </h1>

    <nav data-gw-slot="mainbar" aria-label="Main">
        <ul>
            <li><a href="/" aria-current="page">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/install">Install</a></li>
            <li><a href="/contribute">Contribute</a></li>
            <li><a href="/donate">Donate</a></li>
            <li><a href="/site-map">Site Map</a></li>
        </ul>
    </nav>

    <!-- (...) -->
</gw-mainheader>
```