# Naming structure and organization

## CSS classes naming

Similar to CSS BEM methodology.

```scss
.gw-[block] {
    .gw-[block]-[element] { ... }
    .gw-[block]--[modifier] { ... }
}

.gw--[generic-modifier] { ... }
```

```scss
/* Button pattern with "suggested" modifier */
.gw-button {
    .gw-button--suggested { ... }
}

/* Pagination with "next" and "prev" inner nodes */
.gw-pagination {
    .gw-pagination-next { ... }
    .gw-pagination-prev { ... }
}
```