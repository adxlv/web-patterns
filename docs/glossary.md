# Glossary

## Block
- Reusable pieces of UI: controls, layout containers, navigation elements, etc.
- Example: `.gw-button`, `.gw-content`, `.gw-pagination`, `.gw-icon`

## Element
- Named elements inside a Pattern.
- Example: `.gw-pagination-next`

## Modifier
- Options of a Block. Found as single value or key-value pairs.
- Example: `.gw-box--horizontal`, `.gw-box--gap-small`

## Adaptive Modifier
- Modifiers that are only applied on a predefined viewport media query (`narrow` for mobile browsers, etc).
- Example: `.gw-box--narrow--vertical`

## Generic Modifier
- Modifiers that can be applied anywhere. Used for visual and typographic styles.
- Example: `.gw--title1`, `.gw--caption`, `.gw--theme-dark`, `.gw--pagewidth-small`

## Component
- HTML Web components with custom elements for [Progressive Enhancement](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement) and [ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)
- Example: `<gw-mainheader>`

## Data Attribute
- Used to configure patterns and components. Prefixed as `data-gw-`.
- Example: `data-gw-slot="title"`, `data-gw-icon="home"`

## Slot
- A named region inside a component, usually identified with a Data Attribute: `data-gw-slot="slotname"`

## Viewport
- A predefined viewport media query.
- Example: `narrow` viewport = `width < 768px`