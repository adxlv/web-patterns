---
title: Icons
layout: doc-page
nav_active_item: patterns
---

# Icons

Reusable symbolic icons. May be used independently, or rendered inside a pattern that supports icons.

By default, icons have `1rem` of size, and their colors follow the `default-fg-color`.

Provided icons come from the [Adwaita Icon Theme](https://gitlab.gnome.org/GNOME/adwaita-icon-theme/).


## Usage

Independent use:

```html
<span class="gw-icon" data-gw-icon="icon-name">
    Alt text if icon has semantic meaning
</span>
```

As part of another pattern:

```html
<a href="#" class="gw-button" data-gw-icon="add">Create item</a>
```


## Icon bundles

Icons are bundled in shared SVG files, under `/src/assets/icons`.

### Minimal

Included with every `gw` instance.

<div class="gw-card gw-box gw-box--horizontal gw-box--gap-medium gw-box--wrap">
    <span class="gw-icon" data-gw-icon="gnome">gnome</span>
    <span class="gw-icon" data-gw-icon="menu">menu</span>
    <span class="gw-icon" data-gw-icon="close">close</span>
    <span class="gw-icon" data-gw-icon="home">home</span>
    <span class="gw-icon" data-gw-icon="language">language</span>
    <span class="gw-icon" data-gw-icon="map">map</span>
</div>

### Extending the library

TODO

## Modifiers

### `gw-icon--static`

Sets the icon size using the `px` unit instead of `rem`.


### `gw-icon--large`

Renders the icon at twice the size (`2rem`, or `32px` if static)


## Custom Properties

### `--gw-icon--size`

Sets a custom size.