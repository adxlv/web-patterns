export class GwHeaderbar extends HTMLElement {
    static tagName = "gw-headerbar";

    static define() {
        const tag = GwHeaderbar.tagName;
        if (window.customElements.get(tag)) return;
        customElements.define(tag, this);
    }

    #resizeObserver;
    #viewWidth;
    #viewMode;
    #menuElement;

    get viewWidth() {
        return this.#viewWidth;
    }

    connectedCallback() {
        console.log('HeaderBar added to page.');
        this.setAttribute('role', 'banner');
        
        this.#menuElement = this.querySelector('gw-menu');
        this.#menuElement.addEventListener('focusoutmenu', (e) => {
            this.#menuElement.hidePopover();
        })

        this.#initResizeObserver();
    }

    #initResizeObserver() {
        if (this.#resizeObserver) return;

        this.#resizeObserver = new ResizeObserver((entries) => {
            console.log('resizeObserver', entries);
            if (!entries[0] || !entries[0].contentBoxSize[0]) return;

            const size = entries[0].contentBoxSize[0];
            console.log('new-width', size.inlineSize);
            this.#viewWidth = size.inlineSize;
            this.#updateViewMode();
        });

        this.#resizeObserver.observe(this);
    }

    #updateViewMode() {
        // starts controlling view mode dynamically
        console.log('setWidth');
        const currentViewMode = this.getAttribute('data-gw-viewmode');
        const hasScroll = this.#hasScroll();
        console.log('>> has scroll', hasScroll);

        if (hasScroll) {
            this.setAttribute('data-gw-mainbarhasscroll', 'true');
        } else {
            this.setAttribute('data-gw-mainbarhasscroll', 'false');
        }

        /*
        // FIXME
        
        if (this.#viewWidth < 768) {
            this.setAttribute('data-gw-viewmode', 'narrow');
        } else {
            if (currentViewMode == null || currentViewMode == 'default') {
                if (hasScroll) {
                    this.setAttribute('data-gw-viewmode', 'narrow');
                }
            } else {
                //this.setAttribute('data-gw-viewmode', 'default');
            }
        }
        */
    }

    #hasScroll() {
        // used to check whether the nav has scroll,
        // to add gradient and when to switch to narrow mode

        // return boolean
        const element = this.querySelector('[data-gw-slot="mainbar"]')
        if (!element) return false;

        return element.scrollWidth > element.offsetWidth;
    }
}

GwHeaderbar.define();