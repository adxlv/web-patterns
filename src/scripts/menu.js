export class GwMenu extends HTMLElement {
    static blurMenuItemsEvent = 'focusoutmenu';

    static define(tag = "gw-menu") {
        if (window.customElements.get(tag)) return;
        customElements.define(tag, this);
    }

    connectedCallback() {
        console.log('Menu added to page.');

        this.#currentFocusIndex = 0;

        this.#prepareDom();
        this.#bindEvents();
    }

    #menuRootElement;
    #menuItems;
    #currentFocusIndex;

    #prepareDom() {
        this.#menuRootElement = this.querySelector('ul');
        // Set role="menu"
        this.setAttribute('role', 'menu');

        // Set <ul> and <li> as presentation role
        const presentationItems = this.querySelectorAll('ul, li, hr');
        for (const presentationItem of presentationItems) {
            if (presentationItem.getAttribute('role') === null) {
                presentationItem.setAttribute('role', 'presentation');
            }
        }

        // Set anchor and button roles
        const interactiveItems = this.querySelectorAll('a[href], button');
        for (const interactiveItem of interactiveItems) {
            interactiveItem.setAttribute('tabindex', -1);

            if (interactiveItem.getAttribute('role') === null) {
                interactiveItem.setAttribute('role', 'menuitem');
            }
        }

        this.#prepareMenuItemsDom();
    }

    #bindEvents() {
        // toggle event
        this.addEventListener('toggle', (e) => {
            this.#updateState(e.newState);
        });

        this.addEventListener('keydown', this.#onKeydown.bind(this));

        this.#menuRootElement.addEventListener('focusout', (e) => {
            if (this.#menuRootElement.contains(e.relatedTarget)) return;

            this.#dispatchAllBlurEvent();
        })

        /// TODO: pending behavior:
        /// - dismiss popover when activated
    }


    #prepareMenuItemsDom() {
        this.#menuItems = this.querySelectorAll('[role="menuitem"]:not([aria-disabled="true"])');
    }

    #onKeydown(event) {
        const key = event.key;

        switch (key) {
            case 'ArrowDown':
            case 'Down':
                console.log('down');
                this.focusNextMenuItem();
            break;

            case 'ArrowUp':
            case 'Up':
                console.log('up');
                this.focusPreviousMenuItem();
            break;

            case 'Home':
                this.focusFirstMenuItem();
            break;

            case 'End':
                this.focusLastMenuItem();
            break;

            default:
                return;
        }

        event.stopPropagation();
        event.preventDefault();
    }

    #updateState(newState) {
        console.log('> stateUpdated: ', newState);
        if (newState == 'open') {
            this.focusFirstMenuItem();
        } else {
            this.blurMenuItems();
        }
    }

    #dispatchAllBlurEvent() {
        const event = new Event(GwMenu.blurMenuItemsEvent, {
            bubbles: true,
        });
        this.dispatchEvent(event);
    }

    //#region [ Public API ]
    focusNextMenuItem() {
        if (this.#menuItems.length === 0) return;

        const nextIndex = (this.#currentFocusIndex + 1) % this.#menuItems.length;
        this.focusMenuItem(nextIndex);
        this.#currentFocusIndex = nextIndex;
    }

    focusPreviousMenuItem() {
        if (this.#menuItems.length === 0) return;

        const previousIndexCandidate = this.#currentFocusIndex - 1;
        const previousIndex = previousIndexCandidate < 0 
            ? this.#menuItems.length - 1
            : previousIndexCandidate;
        this.focusMenuItem(previousIndex);
        this.#currentFocusIndex = previousIndex;
    }

    focusFirstMenuItem() {
        if (this.#menuItems.length === 0) return;

        this.#currentFocusIndex = 0;
        this.focusMenuItem(this.#currentFocusIndex);
    }

    focusLastMenuItem() {
        if (this.#menuItems.length === 0) return;

        this.#currentFocusIndex = this.#menuItems.length - 1;
        this.focusMenuItem(this.#currentFocusIndex);
    }

    focusMenuItem(index) {
        if (this.#menuItems[index] === null) return;

        for (const [i, menuItem] of this.#menuItems.entries()) {
            if (i === index) {
                menuItem.setAttribute('tabindex', 0);
                menuItem.focus();
                continue;
            }

            menuItem.setAttribute('tabindex', -1);
        }
        
    }

    blurMenuItems() {
        // set tabindex to -1 for existing items
        const focusableMenuItems = this.querySelectorAll('[tabindex="0"]');
        for (const focusableItem of focusableMenuItems) {
            focusableItem.setAttribute('tabindex', -1);
            focusableItem.blur();
        }

        this.#dispatchAllBlurEvent();
    }
    //#endregion
}

GwMenu.define();